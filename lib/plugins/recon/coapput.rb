
#
# expliot - Internet Of Things Exploitation Framework
# 
# Copyright (C) 2017  Aseem Jakhar
#
# Email: aseemjakhar@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
# BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

require 'expliot'
require 'expliot/protocols/internet/coap'

module Plugins
    module Recon
        class CoapPut < Expliot::TestSuite::RemoteTest

            @about = {
                :name    => "CoAP PUT",
                :id      => self.name.split('::').last.downcase,
                :summary => "Send a PUT request to a CoAP server",
                :author  => "Aseem Jakhar",
                :email   => "aseemjakhar@gmail.com",
                :descr   => "This test sends a PUT request, containing data, to a coap server "  \
                            "on a specified resource path.",
                :ref     => ["https://tools.ietf.org/html/rfc7252", "https://tools.ietf.org/html/rfc6690"],
                :opts    => [Expliot::Commands::Option.get(:rhost),
                             Expliot::Commands::Option.get(:rport,
                                                           "Remote port of service. Default is " \
                                                           "#{Expliot::Protocols::Internet::CoAP::PORT}",
                                                           nil,
                                                           Expliot::Protocols::Internet::CoAP::PORT.to_s),
                            Expliot::Commands::Option.get(:path),
                            Expliot::Commands::Option.get(:data)],
                :proto   => Expliot::Protocols::Internet::CoAP::CoAPClient
            }


            def execute
                puts "[*] Sending CoAP PUT packet to #{@opts[:rhost]} at port #{@opts[:rport]}"
                connect()
                m = @proto.put(@opts[:path], nil, nil, @opts[:data])
                puts "[+] CoAP message received"
                puts "[+] (response_code=#{m.mcode_readable})" \
                         "(message_id=#{m.mid})"               \
                         "(token=#{m.options[:token]})"        \
                         "(content_format=#{CoAP::Registry.convert_content_format(m.options[:content_format])})"
                puts "[+] (payload=#{m.payload})"
            end

        end
    end
end
