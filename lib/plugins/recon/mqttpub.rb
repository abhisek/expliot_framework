
#
# expliot - Internet Of Things Exploitation Framework
# 
# Copyright (C) 2017  Aseem Jakhar
#
# Email: aseemjakhar@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
# BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

require 'expliot'
require 'expliot/protocols/internet/mqtt'

module Plugins
    module Recon
        class MqttPub < Expliot::TestSuite::RemoteTest

            @about = {
                :name    => "MQTT Publish",
                :id      => self.name.split('::').last.downcase,
                :summary => "Publish a message on an MQTT Topic",
                :author  => "Aseem Jakhar",
                :email   => "aseemjakhar@gmail.com",
                :descr   => "This test publishes a message on a topic on an MQTT broker.",
                :ref     => ["http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html"],
                :opts    => [Expliot::Commands::Option.get(:rhost),
                             Expliot::Commands::Option.get(:rport,
                                                           "Remote port of the service. Default port is " \
                                                           "#{Expliot::Protocols::Internet::MQTT::DEFAULT_PORT}",
                                                           nil,
                                                           Expliot::Protocols::Internet::MQTT::DEFAULT_PORT.to_s),
                             Expliot::Commands::Option.get(:topic),
                             Expliot::Commands::Option.get(:msg),
                             Expliot::Commands::Option.get(:id,
                                                          "Specify the client id. Default is a random ID",
                                                           :optional,
                                                           nil)],
                :proto   => Expliot::Protocols::Internet::MQTT::MQTTClient
            }
            # XXX Add retain and qos option
            def execute
                puts "[*] Connecting to MQTT Broker (#{@rhost}) at port (#{@rport})"
                @proto.client_id = @opts[:id]
                connect()
                puts "[*] Publishing the message (#{@opts[:msg]}) to Topic (#{@opts[:topic]})"
                r = @proto.publish(@opts[:topic], @opts[:msg], false, 0)
                puts "[+] Done"
            end
        end
    end
end