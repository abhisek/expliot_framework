
#
# expliot - Internet Of Things Exploitation Framework
# 
# Copyright (C) 2017  Aseem Jakhar
#
# Email: aseemjakhar@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
# BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

require 'serialport'
require 'expliot'

module Plugins

    module Recon

        class BaudScan < Expliot::TestSuite::Test

            DEV         = "/dev/ttyUSB0"
            TIMEOUT     = 5
            WHITESPACE  = [' ', '\t', '\r', '\n']
            PUNCTUATION = ['.', ',', ':', ';', '?', '!']
            VOWELS      = ['a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U']
            BAUDRATES   = [9600, 19200, 38400, 57600, 115200]
            THRESHOLD   = 25

            @about = {
                :name    => "Baudrate Scanner",
                :id      => self.name.split('::').last.downcase,
                :summary => "Scan and identify the baudrate of a serial connection to a device",
                :author  => "Aseem Jakhar",
                :email   => "aseemjakhar@gmail.com",
                :descr   => "Shamelessly stolen from devttys0's baudrate.py. This test allows you to scan and "  \
                            "identify the baudrate of the serial connection to the UART port of a device. "      \
                            "You need to connect the UART port of the device using a USBTTL connector.",
                :ref     => ["https://github.com/devttys0/baudrate"],
                :opts    => [Expliot::Commands::Option.get(:dev, "Device path of the serial port. Default is #{DEV}", nil, DEV),
                             Expliot::Commands::Option.get(:timeout,
                                                           "Read timeout in seconds. Default is #{TIMEOUT}",
                                                            nil,
                                                            TIMEOUT),
                             Expliot::Commands::Option.get(:threshold,
                                                           "Threshold bytes to read per baud rate. Default is #{THRESHOLD}",
                                                           nil,
                                                           THRESHOLD),
                             Expliot::Commands::Option.get(:verbose)]
            }

            #
            # Checks whether the passed baud rate is correct for the serial connection
            #
            def found?(baud)
                SerialPort.open(@opts[:dev], baud) { |s|
                    ws   = 0
                    punc = 0
                    vow  = 0
                    puts "[*] Checking baud rate #{baud}"
                    s.read_timeout = @opts[:timeout].to_i * 1000
                    for i in 1..@opts[:threshold].to_i do
                        c = s.getc
                        if c == nil
                            break
                        end
                        printf(c)
                        if c.ascii_only?
                            if WHITESPACE.include? c
                                ws +=1
                            elsif PUNCTUATION.include? c
                                punc += 1
                            elsif VOWELS.include? c
                                vow += 1
                            end
                        end
                    end
                    puts ""
                    if ws > 0 and punc > 0 and vow > 0
                        return true
                    end
                }
                return false
            end

            def execute
                puts "[*] Scanning for correct baud rate of the serial connection"
                BAUDRATES.each {|baud|
                    if found?(baud)
                        puts "[+] FOUND! Baudrate = #{baud}"
                        break
                    end
                }
            end


        end
    end
end