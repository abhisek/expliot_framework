
#
# expliot - Internet Of Things Exploitation Framework
# 
# Copyright (C) 2017  Aseem Jakhar
#
# Email: aseemjakhar@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
# BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

module Expliot

	module TestSuite

		#
		# The repository hash of all the test classes. This has to be loaded
		# individually by each class in its definition (outside the initialize method)
		# When the tests need to be run. Do a each loop and instantiate the class objects
		# which can then be used directly
	    #class << self; attr_accessor :suite; end
	    @@suite = {}

	    def self.load_suite
	    	if @@suite.empty?
	        	@@suite = Test.descendants
	        	#puts "Suite #{@@suite}"
	        	#@@suite.each {|testid, klass|
                #	puts "Test added in suite (id [#{testid}]) (class [#{klass}])"
	        	#}
	    	end
	    end

	    def self.suite
	    	@@suite
	    end

	    # Base class for all test cases
        class Test
			public
			attr_accessor :name, :id, :summary, :description, :author, :email, :reference, :opts, :poc, :success
		    @name = "Test case Base class"
		    @id = "Unique_Test_class_id"
		    @summary = "Test case summary"
		    @description = "Long Test case description"
		    @author = "Rakkhi Sawant"
		    @email  = "rakkhi@example.com"
		    @ref = "Online Reference of this test case"
		    #@opts   = [:rhost => "Hash containing options and their values specified by the users(or default)"]
		    @poc = "Proof of concept data of the test case for report/display"
		    @success = false # Boolean value specifying whether test case was successful or not

            @about = {:name    => nil,  # String - User friendly name of the Test case
            	      :id      => nil,  # String(no space) - ID to be used for identifying and showing test case to user
            	      :summary => nil, # String - Summary displayed in help and listing
            	      :author  => nil, # String - Author's Full name
            	      :email   => nil, # String - Author's email ID
            	      :descr   => nil, # String - Long description of the Test case
            	      :ref     => nil, # Array - Reference to the CVE, source of the test case etc
                      :opts    => nil, # Hash - Hash of option names and their default values
                      # XXX remove poc and success and make them instance variables in initialize
                      :poc     => nil, # String - Test poc communication/data to be put in the report
                      :success => nil} # Boolean - Test status. true = success, false = failure             

            # XX Remove this method. Its not required
            def self.define(about)
            	@about[:name] = about[:name]
            	@about[:id] = about[:id]
            	@about[:summary] = about[:summary]
            	@about[:descr] = about[:descr]
            	@about[:author] = about[:author]
            	@about[:email] = about[:email]
            	@about[:ref] = about[:ref]
            	@about[:opts] = about[:opts]
            	@about[:poc] = about[:poc]
            	@about[:success] = about[:success]
            end

            def self.about(key)
            	@about[key]
            end

            #
            # Get all the descendant classes of the base Test class which can be instantiated.
            # These are all the plugins that implement a test case. This is required to load
            # all the plugins in the TestSuite
            #
            def self.descendants
            	repo = {}
            	ObjectSpace.each_object(Class).select { |klass| 
            		if klass < self then
                        if self.instantiable? klass
                        	#puts "#{klass} class can be instantiated. Id = (#{klass.about(:name)})"
                            repo[klass.about(:id)] = klass
                        #else 
                        	#puts "#{klass} cannot be instantiated"
                        end
            		end
            	}
            	repo
            end

            # XXX Change the logic to check if abstract Tes classes can be instantiated or not
            # This function checks if a Test inherited class can be instantiated. All test cases
            # are descendents of Test class and have to be instantiated. Only the Abstract Test
            # classes defined here from which actual test cases inherited cannot be instantiated
            def self.instantiable? (klass)
            	abstract = [Test, RemoteTest]
            	not abstract.include? klass
            end

		    def initialize
		    	# XXX Change this logic. The about Test information stays in class instance @about
		    	# and not the object instance as @about is not an object member variable (as per ruby)
		    	#
		    	#
		    	# Fill @opts with option long name as key and their default values/nil as the data
		    	@opts = {}
		    	self.class.about(:opts).each { |opt|
		    		@opts[opt.long] = opt.default

		    	}
                # No need to dup this as id is a readonly value anyway
                @name    = self.class.about(:name)
		    	@id      = self.class.about(:id)
		    	@summary = self.class.about(:summary)
            	@descr   = self.class.about(:descr)
            	@author  = self.class.about(:author)
            	@email   = self.class.about(:email)
            	@ref     = self.class.about(:ref)
            	#@about[:poc] = about[:poc]
            	#@about[:success] = about[:success]

		    	
		    end

            #
            # Setup method that can be implemented for any pre-conditions
            # Make sure to call super() in the implementation that inherits this class
            #
		    def pre
		    	@opts.each {|k, v|
		    		# .empty? will only work for strings and not for true/false values
                    #if @opts[k] == nil || @opts[k].empty?
                    # Check if argument value is nil
                    if @opts[k] == nil
                    	# If the argument is required then raise Exception
                    	self.class.about(:opts).each { |o|
                    		if o.long == k and o.argtype == :required
                    			raise ArgumentError.new("#{@id}: #{k} argument not specified")
                    		end
                        }
        			end
		    	}
		    	puts "[*]"
		    	puts "[*] Test:         #{@name}"
		    	puts "[*] Author:       #{@author} Email: #{@email}"
		    	puts "[*] Reference(s): #{@ref}"
		    	puts "[*]"
		    end

            # The actual method that executes the test case
		    def execute
		    end

            #
		    # Tear down method that can be implemented for any post conditions
		    # Make sure to call super() in the implementation that inherits this class
            #
		    def post
		    end
		end

		class RemoteTest < Test
			# XXX This is not used anywhere till now. Remove
            def self.define(about)
            	super(about)
            	@about[:proto] = about[:proto]
            end

		    def initialize()
		    	super()
		    	@rbuflen = 1024
		    	@proto = self.class.about(:proto).new
		    	@payload = nil
		    end

		    def connect
		    	if @proto.respond_to? :connect
		    		@proto.connect(@rhost, @rport)
		    	else
		    		raise NotImplementedError.new("#{@id}: #{:connect} method not implemented by Protocol #{@proto.class}")
		    	end
		    end

            def send
            	if @proto.respond_to? :send
            		@proto.send(@payload, 0)
            	else
            		raise NotImplementedError.new("#{@id}: #{:send} method not implemented by Protocol #{@proto.class}")
            	end
            end

            def recv
            	if @proto.respond_to? :recv
            		@proto.recv(@rbuflen)
            	else
            		raise NotImplementedError.new("#{@id}: #{:recv} method not implemented by Protocol #{@proto.class}")
            	end
            end

		    def close
		    	if @proto.respond_to? :close
		    		@proto.close()
		    	end
		    end

		    def pre
		    	super()
		    	@rhost = @opts[:rhost]
		    	@rport = @opts[:rport]
		    end

		    def post
		    	super()
		    	close()
		    end
		end
	end
end

