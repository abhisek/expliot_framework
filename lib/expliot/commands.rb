
#
# expliot - Internet Of Things Exploitation Framework
# 
# Copyright (C) 2017  Aseem Jakhar
#
# Email: aseemjakhar@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
# BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

require 'cri'
require 'expliot/testsuite'

module Expliot

    module Commands

        class Option
            attr_accessor :short, :long, :descr, :argtype, :default

            #
            # initialize method of class Option
            # @param short   Short name. It is a single character ex. h, v, b etc. Dont use this
            #                Just set it to nil
            # @param long    Long name. It is usually a word ex. help, verbose, path etc. Must not be nil
            # @param descr   Description of the Option. include the value of default as well if it is not nil
            # @param argtype argument type. Valid values are one of :required, :optional or :forbidden
            #                i.e. whether the argument is required or not
            # @param default Default value of the option if any, nil otherwise

            #def update(ohash)
            #   ohash.each {|k, v|
            #       case k
            #       when :short
            #           @short = v
            #       when :long
            #           if v.nil?
            #               raise ArgumentError.new("Option long name not specified")
            #           end
            #           @long = v
            #       when :descr
            #           if v.nil?
            #               raise ArgumentError.new("Option description not specified")
            #           end
            #           @descr = v
            #       when :argtype
            #           if ![:required, :optional, :forbidden].include? v
            #               raise ArgumentError.new("Unknoen Option argument type (#{argtype})")
            #           end
            #           @argtype = v
            #       when :default
            #           @default = v
            #       else
            #           raise ArgumentError.new("Unknown Option argument (#{k})")
            #       end
            #   }
            def initialize(short, long, descr, argtype, default)
                if long.nil?
                    raise ArgumentError.new("Option long name not specified")
                elsif descr.nil?
                    raise ArgumentError.new("Option description not specified")
                elsif !self.class.argtype_valid? argtype
                    raise ArgumentError.new("Unknoen Option argument type (#{argtype})")
                end
                @short   = short
                @long    = long
                @descr   = descr
                @argtype = argtype
                @default = default
            end

            def self.argtype_valid?(a)
                [:required, :optional, :forbidden].include? a
            end

            #
            # Returns the option object from the internal repository OPTIONS
            # dups it if we are editing the description and/or argtype and/or the default variable
            #
            # @param opt The option (name) from the internal repository OPTIONS
            # @param descr   Description of the Option. include the value of default as well if it is not nil
            # @param argtype argument type. Valid values are one of :required, :optional or :forbidden
            #                i.e. whether the argument is required or not
            # @param default Default value of the option if any, nil otherwise
            # @return the Option object 

            def self.get(opt, descr = nil, argtype = nil, default = nil)
                return nil if OPTIONS[opt].nil?
                o = OPTIONS[opt]
                o = o.dup unless (descr.nil? and argtype.nil? and default.nil?)

                if !descr.nil?
                    o.descr = descr
                end
                if !argtype.nil?
                    raise ArgumentError.new("Unknown Option argument type (#{argtype})") unless self.argtype_valid? argtype
                    o.argtype = argtype
                end
                if !default.nil?
                    o.default = default
                end
                return o
            end

            #
            # Returns a new Option object from internal option respository and
            # changes updates its default member variable
            #
            # @param opt     The option present in the internal reposiroty OPTIONS
            # @param default The default value (String) of the option
            # @return Option object
            #def self.update_default(opt, default)
            #   o = OPTIONS[opt].dup
            #   o.default = default unless o.nil?
            #   return o
            #end
        end

        # XXX Currently (3 April 2017) most of the options are required and id is optional. This is for
        # compatibility with CRI. But we may have diff. tests needing different argument requirements for
        # the same argument i.e. id may be optional in one case and required in another. Fix that in next release
        OPTIONS = {:rhost     => Option.new(:r, :rhost, "Remote host or IP", :required, nil).freeze,
                   :rport     => Option.new(nil, :rport,     "Remote port number of the service", :required, nil).freeze,
                   :lhost     => Option.new(:l,  :lhost,     "Local host or IP", :required, nil).freeze,
                   :lport     => Option.new(nil, :lport,     "Local port number of the service", :required, nil).freeze,
                   :pass      => Option.new(:p,  :pass,      "Password", :required, nil).freeze,
                   :topic     => Option.new(nil, :topic,     "Specify the Topic", :required, nil).freeze,
                   :mac       => Option.new(:a,  :mac,       "Mac address. Format - 00:00:de:ad:be:ef", :required, nil).freeze,
                   :switch    => Option.new(:s,  :switch,    "Switch on or off. Valid arguments are either on or off", :required, nil).freeze,
                   :dev       => Option.new(:d,  :dev,       "The device path", :required, nil).freeze,
                   :count     => Option.new(:c,  :count,     "Specify the count", :required, nil).freeze,
                   :timeout   => Option.new(:t,  :timeout,   "Specify a timeout value in seconds", :required, nil).freeze,
                   :threshold => Option.new(nil, :threshold, "Specify a threshold value", :required, nil).freeze,
                   :path      => Option.new(nil, :path,      "Specify the path", :required, nil).freeze,
                   :data      => Option.new(nil, :data,      "Specify the data", :required, nil).freeze,
                   :msg       => Option.new(:m,  :msg,        "Specify the message", :required, nil).freeze,
                   :id        => Option.new(:i,  :id,        "Specify the id", :required, nil).freeze,
                   # BLE options
                   :handle    => Option.new(nil,  :handle,    "Specify the handle for read/write", :required, nil).freeze,
                   :value     => Option.new(nil,  :value,    "Specify the value to write", :required, nil).freeze,
                   # Options without arguments
                   :help      => Option.new(:h,  :help,      "show help for this command", :forbidden, nil).freeze,
                   :verbose   => Option.new(:v,  :verbose,   "verbose mode", :forbidden, nil).freeze
        }

        #OPTIONS = {# Options with arguments
        #          :rhost     => [nil, :rhost,     "Remote host or IP",                                      argument: :required],
        #          :rport     => [nil, :rport,     "Remote port number of the service",                      argument: :required],
        #          :lhost     => [:l,  :lhost,     "Local host or IP",                                       argument: :required],
        #          :lport     => [nil, :lport,     "Local port number of the service",                       argument: :required],
        ##         :topic     => [nil, :topic,     "Specify the Topic",                                      argument: :required],
        #          :mac       => [:a,  :mac,       "Mac address. Format - 00:00:de:ad:be:ef",                argument: :required],
        #          :switch    => [:s,  :switch,    "Switch on or off. Valid arguments are either on or off", argument: :required],
        #          :dev       => [:d,  :dev,       "The device path",                                        argument: :required],
        #          :timeout   => [:t,  :timeout,   "Specify a timeout value in seconds",                     argument: :required],
        #          :threshold => [nil, :threshold, "Specify a threshold value",                              argument: :required],
        #          :path      => [nil, :path,      "Specify the path",                                       argument: :required],
        #          :data      => [nil, :data,      "Specify the data",                                       argument: :required],
        #          :msg       => [:m, :msg,        "Specify the message",                                    argument: :required],
                   # Options without arguments
        #          :help      => [:h,  :help,      "show help for this command",                             argument: :forbidden],
        #          :verbose   => [:v,  :verbose,   "verbose mode",                                           argument: :forbidden]}

        @@efcmd = nil
        @@runcmd = nil

        # XXX This is not used delete it
        class TestRunner < Cri::CommandRunner
            attr_accessor :test
            @test = nil

            def initialize(options, arguments, command, test)
                super(options, arguments, command)
                @test = test
            end

            def run
                if not @options[:help]
                    puts "opts (#{@options})"
                    puts "args (#{@arguments})"
                    puts "cmd (#{@command})"
                    @test.pre(@options)
                    @test.execute
                    @test.post
                end
            end

        end
        # Extending the Command class with Test object instance variable
        # The test object will be responsible for execution of the test case
        class Cri::Command
            attr_accessor :test

            @test = nil
        end

        # Factory class responsible for creating commands for each Test and the
        # Top level commands
        class CommandFactory

            @@helpblock = Proc.new {|val, cmd| puts cmd.help}
          
            def self.create_testcmd(test, addhelp)
                cmddsl = self.create_dsl(test.about(:id),
                                         nil,
                                        "#{test.about(:id)} [options]",
                                        test.about(:summary),
                                        test.about(:descr),
                                        test.about(:opts),
                                        addhelp)
                cmddsl.run {|opts, args, cmd|
                    testobj = cmd.test.new
                    #puts "Instantiating in run block class #{testobj}"
                    opts.each {|opt, val|
                        if val != nil
                            #puts "#{opt} => #{val}"
                            testobj.opts[opt] = val
                            #puts "IN object val = #{testobj.opts[opt]}"
                        end
                    }
                    # XXX remove the check for help when Cri new version fixes the issue. Bug = run 
                    # block also executes even if help option specified
                    if not opts[:help]
                        #puts "opts (#{opts})"
                        #puts "args (#{args})"
                        #puts "cmd (#{cmd.name})"
                        testobj.pre
                        testobj.execute
                        testobj.post
                        #cmd.test.pre
                        #cmd.test.execute
                        #cmd.test.post
                    end
                }
                cmddsl.command.test = test
                #cmddsl.command.supercommand = parentcmd
                cmddsl.command
            end

            # Creates and returns a CommandDSL which can be used to create final command
            def self.create_dsl(name, aliases, usage, summary, description, options, addhelp)
                dsl = Cri::CommandDSL.new
                dsl.name(name)
                #dsl.aliases(aliases)
                dsl.usage(usage)
                #dsl.aliases(:c)
                dsl.summary(summary)
                dsl.description(description)
                if options != nil and options.empty? != true                    
                    options.each { |optobj|
                        dsl.option(optobj.short,
                                   optobj.long,
                                   optobj.descr(),
                                   argument: optobj.argtype)
                    }
                end
                if addhelp then
                    dsl.option(OPTIONS[:help].short,
                               OPTIONS[:help].long,
                               OPTIONS[:help].descr,
                               argument: OPTIONS[:help].argtype,
                               &@@helpblock)
                end
                dsl
            end

            # Create a generic command
            def self.create_generic(name, aliases, usage, summary, description, options, addhelp, &block)
                dsl = self.create_dsl(name,
                                      aliases,
                                      usage,
                                      summary,
                                      description,
                                      options,
                                      addhelp)
                dsl.run &block
                #dsl.run {|opts, args, cmd|
                                # XXX This is fixed in new version of CRI. Remove the help check
                                # Same as create_testcmd
                #                if not opts[:help]
                #                        puts "#{cmd.name}: Nothing to do"
                #                end
                #        }
                dsl.command
            end
        end

        
        # Initializes and returns the run command 
        def self.init_runcmd
            blk = Proc.new {|opts, args, cmd|
                    # XXX This is fixed in new version of CRI. Remove the help check
                    # Same as create_testcmd
                    if not opts[:help]
                        puts "#{cmd.name}: Nothing to do. Try with -h or --help"
                    end
                }
            @@runcmd = CommandFactory.create_generic("run",
                                                     :r,
                                                     "run [options]",
                                                     "Execute a test case",
                                                     "This command is responsible for executing individual test cases",
                                                     nil,
                                                     true, &blk)

            # Load the test suite and create corresponding sub commands
            Expliot::TestSuite.load_suite
            Expliot::TestSuite.suite.each {|id, klass|
                cmd = CommandFactory.create_testcmd(klass, true)
                @@runcmd.add_command(cmd)
            }
            @@runcmd
        end

        # Initialize the help command
        def self.init_helpcmd
            hblk = Proc.new {|opts, args, cmd|
                       puts "List of Commands:"
                       puts "  run  - Execute a Test case. Use with -h or --help for more details"
                       puts "  exit - Exit the expliot program"
                       puts "  help - This help command"
                   }
            @@helpcmd = CommandFactory.create_generic("help",
                                                     "h",
                                                     "help",
                                                     "The Help command",
                                                     "The help command shows a list of all the commands supported by expliot",
                                                     nil,
                                                     false,
                                                     &hblk)
            @@helpcmd
        end

        # Initialize the exit command
        def self.init_exitcmd
            @@exitcmd = CommandFactory.create_generic("exit",
                                                      "e",
                                                      "exit",
                                                      "The exit command",
                                                      "The exit command exits the expliot program",
                                                      nil,
                                                      true,
                                                      &(Proc.new {|opts, args, cmd| exit(0)}))
            @@exitcmd
        end

        # Initializes all the commands and returns the top level command 
        def self.init_commands(prompt)
            # Stub run block for top level command i.e. ef>
            blk = Proc.new {|opts, args, cmd| }
            @@efcmd = CommandFactory.create_generic(prompt,
                                                    :e,
                                                    "#{prompt} [commands]",
                                                    "Expliot Framework",
                                                    "expliot - Internet Of Things Exploitation Framework",
                                                    nil,
                                                    false,
                                                    &blk)
            @@efcmd.add_command(self.init_runcmd)
            @@efcmd.add_command(self.init_helpcmd)
            @@efcmd.add_command(self.init_exitcmd)
            @@efcmd
        end

    end
end