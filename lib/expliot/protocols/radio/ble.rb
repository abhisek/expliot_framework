
#
# expliot - Internet Of Things Exploitation Framework
# 
# Copyright (C) 2017  Aseem Jakhar
#
# Email: aseemjakhar@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
# BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

module Expliot

    module Protocols

        module Radio

            module BLE
                # XXX Fix me. Parse the input and remove special characters to avoid
                # command execution. Not a big issue since the user runs via console and
                # and better know what he/she is doing :)
                require 'expliot/protocols/radio/ble/hcitool'
                require 'expliot/protocols/radio/ble/gatttool'

                def self.lescanall(iface = "hci0", timeout = 10)
                    puts "doing lescan"
                    devs = self.simple_lescan(timeout)
                    devs.each do |d|
                        d.primary = GATTTool.scan_primary(d.addr, iface)
                        d.characteristics = GATTTool.scan_characteristics(d.addr, iface)
                    end
                    return dhash
                end

                #
                # Performs a basic lescan for timeout seconds
                #
                # @param timeout stop scanning after timeout seconds, default 10 secs
                #
                # @return Array of found Device objects
                def self.lescan_basic(timeout = 10)
                    dhash = {}
                    r = HCITool.lescan(timeout)
                    r.each do |l|
                        arr = l.split(' ')
                        if !arr.nil? and !arr[1].nil?
                            # Add if not present or if the name is not "(Unknown)"
                            # hcitool lescan behaves wierd, gives the name unknown for the same addr
                            # for wich it previously printed the actual device name
                            if dhash[arr[0]].nil? or arr[1].match(/\(unknown\)/i).nil?
                                dhash[arr[0]] = Device.new(arr[0], arr[1])
                            end
                        end
                    end
                    return dhash.values
                end

                class Device
                    attr_accessor :addr, :name, :primary, :characteristics

                    def initialize(bdaddr, name)
                        @addr = bdaddr
                        @name = name
                        @primary = []
                        @characteristics = []
                    end
                end
            end
        end
    end
end