
#
# expliot - Internet Of Things Exploitation Framework
# 
# Copyright (C) 2017  Aseem Jakhar
#
# Email: aseemjakhar@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
# BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

require 'pty'

module Expliot

    module Protocols

        module Radio

            module BLE

                class HCITool

                    CMD = "hcitool"

                    # Scans for bluetooth devices
                    #
                    # XXX: hcitool scan --info requires root privileges
                    #
                    # @param len    inquiry time in seconds, set to zero to use hcitool default time
                    # @param nrsp   Max number of responses, set to zero to use hcitool default
                    # @param flush  flush the old devices cache and scan.
                    # @param clas   Get device class information
                    # @param info   Get detailed information about the devices like LMP version and features
                    # @param oui    Output OUI and company information of the BD address
                    # @param lap    Inquiry access code in hex string format i.e "0x9e8b33"
                    #
                    # @return  Array containing the output of hcitool scan
                    def self.scan(len = 0, nrsp = 0, flush = false, clas = false, info = false, oui = false, lap = nil)

                        cmd = [CMD]
                        cmd.push("scan")
                        cmd.push("--length=#{len}") if len != 0
                        cmd.push("--numrsp=#{nrsp}") if nrsp != 0
                        cmd.push("--flush") if flush == true
                        cmd.push("--class") if clas == true
                        if info == true
                            raise Errno::EPERM.new("hcitool scan --info requires root privileges") if !Process.uid.zero?
                            cmd.push("--info")
                        end
                        cmd.push("--oui")   if oui == true
                        cmd.push("--iac=#{lap}") if lap != nil
                        proc = IO.popen(cmd)
                        rarray = proc.readlines
                        # Remove the first line from output - "Scanning ..."
                        rarray.shift if !rarray.nil? and rarray[0].match(/scanning/i)        
                        proc.close()
                        return rarray
                    end

                    # Scans for BLE devices
                    #
                    # XXX: hcitool lescan keeps on scanning and does not exit so we need to close it ourselves
                    #      If hcitool receives a signal other than SIGINT, it does not run again and gives the
                    #      error - "Set scan parameters failed: Input/output error". If you see this error
                    #      disable/enable the adapter and it should work fine.
                    #      This needs to be fixed
                    # XXX: hcitool lescan requires root privileges
                    #
                    # @param timeout    timeout to kill hcitool command
                    # @param privacy    Enable privacy
                    # @param passive    Set scan type passive (default active)
                    # @param whitelist  Scan for address in the whitelist only
                    # @param discovery  Enable general or limited discovery procedure (valid values "g" or "l"
                    # @param duplicates Don't filter duplicates
                    #
                    # @return  Array containing the output of hcitool scan
                    def self.lescan(timeout = 10, privacy = false, passive = false, whitelist = false, discovery = nil, duplicates = false)
                        rarray = []

                        raise Errno::EPERM.new("hcitool lescan requires root privileges") if !Process.uid.zero?
                        cmd = [CMD]
                        cmd.push("lescan")
                        cmd.push("--privacy") if privacy == true
                        cmd.push("--passive") if passive == true
                        cmd.push("--whitelist")  if whitelist == true
                        cmd.push("--discovery=#{discovery}") if discovery == 'g' or discovery == 'l'
                        cmd.push("--duplicates") if duplicates == true

                        # Use PTY so we can get stdout in realtime instead of buffered as in popen()
                        # which buffers the output for sometime and then sends it
                        r, w, pid = PTY.spawn(cmd.join(" "))
                        a = Time.now
                        while (Time.now - a < timeout)
                            line = r.gets
                            rarray.push(line) if !line.nil?
                        end
                        rarray.shift if !rarray.nil? and rarray[0].match(/le\s+scan/i)
                        #puts "send SIGINT to hcitool(pid=#{pid}) to stop it gracefully"
                        # Process.kill() takes a lot of time to return, assuming it does some other checks
                        # which are causing ruby to send some other signal than SIGINT to the child (hcitool)
                        # and hcitool fails to rerun. But if we kill using system() it
                        # it works fine i.e. gets SIGINT and exits properly
                        #Process.kill("SIGINT", proc.pid)
                        system("kill -2 #{pid}")
                        r.close()
                        w.close()
                        return rarray
                    end
                end
            end
        end
    end
end