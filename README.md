# Expliot #
(Pronounced - expl-aa-yo-tee)

Internet Of Things Exploitation Framework
--

Expliot is a framework for seucurity testing IoT and IoT infrastructure. It provides a set of plugins (test cases) and can be extended easily to create new plugins.

### Install ###

* sudo gem install bundler
* sudo apt-get install ruby-dev
* git clone <expliot_repo>
* cd expliot
* bundle install

### Run ###
* cd expliot
* ./efconsole.rb
* ef> run -h

### Contribution ###
* Suggesting new plugins/test cases 
* Sharing any vulnerability information that can be translated to a plugin
* Please do not submit a patch, instead send me an email about what you have in mind

### Author ###
* Name: Aseem Jakhar
* Twitter: [@aseemjakhar](https://twitter.com/aseemjakhar)
* Linkedin: [Aseem Jakhar](https://in.linkedin.com/in/aseemjakhar)

### Contact ###
* aseemjakhar AT gmail DOT com

### Huge shout out to ###
* The one and only computer pirate ;)
* [null - The open security community](http://null.co.in)
* [Abhisek Datta](https://twitter.com/abh1sek)
* [Javier Vazquez Vidal](https://twitter.com/fjvva)
* [Milosch Meriac](https://www.meriac.com/)
* [Payatu Bandits](http://www.payatu.com/)
* [Hardwear.io Conference](https://hardwear.io/)
* [nullcon Conference](http://nullcon.net/)
